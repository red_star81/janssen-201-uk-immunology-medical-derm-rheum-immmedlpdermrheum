(function(window, $) {

'use strict';

// Document -------------------------------------------------------------------

// When using jQuery, use
$(document).ready(function () {

	console.log('## Document ready');

	// VARS -------------------------------------------------------------------
	//

	var
		$window = $(window),
		$body 	= $('body'),
		$form 	= $('#register-form'),
		$emailField = $('#email-field'),
		$submitBtn = $('button[type=submit]'),
		$labels = $('label'),
		$requiredFields = $('input[required], select[required]'),
		$checkRadio =$('input[type=radio]'),
		$errorList = $('ul.report--error'),
		emailFilter = /.*[@]{1}.*[\.](.*){2,4}$/
	;

	// ACTIONS ----------------------------------------------------------------
	//

	$form.on('submit', validateForm);

	//
	// FUNCTIONS --------------------------------------------------------------
	//

	function validateForm (e) {

		var isFormValid = true;

		$errorList.hide().html('');
		$labels.removeClass('is-invalid');

		if($('input[name=terms]:checked').length === 0 ) {
			$('input[name=terms]').parent().addClass('is-invalid');
			isFormValid = false;
		}

		$requiredFields.each(function(key, item) {

			item = $(item);

			if(item[0].tagName === 'SELECT') {
				if ( $.trim(item.val()).length === 0 ) {
					item.parent().parent().addClass('is-invalid');
					isFormValid = false;
				}

			}
				else {
				if ( $.trim(item.val()).length === 0 ) {
					item.parent().addClass('is-invalid');
					isFormValid = false;
				}
			}
		});

		$checkRadio.each(function(key1, item1) {

				item1 = $(item1);
				var unique = [];
				var unchecked = item1.find('tr:not(:first-child):not(:has(:radio:checked))');
				var rowCount = item1.find('tr:not(:first-child)').length;

				console.log(rowCount + " " + unchecked);

				// Check if ranking contains 'other' field
				if(item1.find('.other').length && $.trim(item1.find('.other input[type=text]').val()).length === 0 ) {
					rowCount = item1.find('tr:not(:first-child):not(.other)').length;
					unchecked = item1.find('tr:not(:first-child):not(.other):not(:has(:radio:checked))');

					console.log(rowCount + " " + unchecked);
				}

				// Check for unique checked values
				$.each(item1.find('input[type=radio]:checked'), function(key, item) {
					unique.push(item.value);
				});

				// Check if all radiobutton groups are checked
				if( unchecked.length !== 0 ) {

					showError('Some items were not ranked', item1);
					formValid = false;

				// Check if every checked value is unique
				} else if( $.unique(unique).length < rowCount ) {

					showError('Ranked values should be unique', item1);
					formValid = false;

				} else {

					formValid = true;
				}
		});

		if(!isFormValid) {
			showError('Please fill in the required fields.');
			e.preventDefault();
			return false;
		}

		if (!emailFilter.test($.trim($emailField.val()))) {
			$emailField.parent().addClass('is-invalid');
			showError('Please fill in a correct email address.');
			
			e.preventDefault();
			return false;
		}
	}

	function showError (err) {

		if (!$errorList.length) {
			$submitBtn.parent().before('<ul class="report--error"></ul>');
			$errorList = $($errorList.selector);
		}

		$errorList.show().append('<li>' + err + '</li>');
	}

});

}(window, $));